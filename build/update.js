#!/usr/bin/env node

/*
 * This file is part of @adblockinc/rules <https://adblock.org/>,
 * Copyright (C) 2021-present Adblock Inc.
 *
 * @adblockinc/rules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @adblockinc/rules is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @adblockinc/rules.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as cli from "./cli.js";
import * as fsh from "./fs-helper.js";
import {buildIndex, updateIndex} from "./index-builder.js";
import {execNpm} from "./utils.js";

const addonName = cli.getAddonName();
fsh.setAddonName(addonName);

await updateIndex();
await buildIndex(addonName);

await execNpm(
  "subs-fetch",
  "-i", await fsh.getDistPath(fsh.indexSelected),
  "-o", await fsh.getDataPath(fsh.rulesAbp)
);
console.log("Updated rules", fsh.getAddonPath(fsh.rulesAbp));
