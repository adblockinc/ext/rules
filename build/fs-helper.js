/*
 * This file is part of @adblockinc/rules <https://adblock.org/>,
 * Copyright (C) 2021-present Adblock Inc.
 *
 * @adblockinc/rules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @adblockinc/rules is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @adblockinc/rules.  If not, see <http://www.gnu.org/licenses/>.
 */

import {promises as fs} from "fs";
import url from "url";
import path from "path";

const scriptDir = path.dirname(url.fileURLToPath(import.meta.url));
const rootDirectory = path.join(scriptDir, "..");
const dataDirectory = path.join(rootDirectory, "data");
const distDirectory = path.join(rootDirectory, "dist", "%ADDON%");

export const indexEyeo = path.join("index", "eyeo.json");
export const indexSelected = path.join("index", "%ADDON%.json");
export const manifestFragment = path.join("manifest", "%ADDON%.json");
export const rulesAbp = path.join("rules", "abp");
export const rulesDnr = path.join("rules", "dnr");

let addonName = null;

export async function clean()
{
  try
  {
    await fs.rm(getAddonPath(distDirectory), {recursive: true});
  }
  catch(ex)
  {
    // Ignore if directory doesn't exist yet
  }
}

export function getAddonPath(fileReference)
{
  if (!addonName)
    throw new Error("Missing addon name");

  return fileReference.replaceAll("%ADDON%", addonName);
}

async function mkdirp(basePath, fileReference)
{
  const absolutePath = getAddonPath(path.join(basePath, fileReference));
  const dirPath = (path.extname(absolutePath))
    ? path.dirname(absolutePath)
    : absolutePath;
    
  try
  {
    await fs.access(dirPath);
  }
  catch(ex)
  {
    if (ex.code !== "ENOENT")
    {
      console.error(ex);
      throw ex;
    }

    await fs.mkdir(dirPath, {recursive: true});
  }

  return absolutePath;
}

export async function readJson(filepath)
{
  const content = await fs.readFile(filepath, "utf8");
  return JSON.parse(content);
}

export function setAddonName(newAddonName)
{
  addonName = newAddonName;
}

export const getDataPath = mkdirp.bind(null, dataDirectory);
export const getDistPath = mkdirp.bind(null, distDirectory);
