#!/bin/bash

set -eux

glab auth login --token $GITLAB_TOKEN_EXTENSIONS

git clone https://$GITLAB_TOKEN_EXTENSIONS_NAME:$GITLAB_TOKEN_EXTENSIONS@gitlab.com/eyeo/browser-extensions-and-premium/extensions/extensions.git extensions

OLD_COMMIT_SHA=$(jq -r '.dependencies."@adblockinc/rules" | match("#([0-9a-f]+)").captures[0].string' extensions/host/adblock/package.json)

echo -e "build: Updated @adblockinc/rules dependency to update rules data [noissue]\n" > commit_message.txt

echo -e "## Change log:\n\`\`\`" >> commit_message.txt
git log --oneline $OLD_COMMIT_SHA..$CI_COMMIT_SHA >> commit_message.txt
echo -e "\`\`\`\n" >> commit_message.txt

echo -e "## Rules file changes:\n\`\`\`" >> commit_message.txt
git diff --stat $OLD_COMMIT_SHA..$CI_COMMIT_SHA >> commit_message.txt
echo -e "\`\`\`\n" >> commit_message.txt

cd extensions

BRANCH_NAME="mr-automaton-latest-rules"
git checkout -B $BRANCH_NAME HEAD

npm install --ignore-scripts -w host/adblock gitlab:$CI_PROJECT_PATH#$CI_COMMIT_SHA
npm install --ignore-scripts -w host/adblockplus gitlab:$CI_PROJECT_PATH#$CI_COMMIT_SHA

if [ "$(git status --porcelain)" ]; then
    git add --update
    git commit -F ../commit_message.txt
    git push --force origin $BRANCH_NAME

    glab mr close $BRANCH_NAME || true
    glab mr create --fill --squash-before-merge --remove-source-branch --yes
fi
