#!/bin/bash

set -eux

glab auth login --token $GITLAB_TOKEN_RULES
git remote set-url --push origin "https://$GITLAB_TOKEN_RULES_NAME:$GITLAB_TOKEN_RULES@gitlab.com/$CI_PROJECT_PATH.git"

npm ci
npm run update adblock
npm run update adblockplus

if [ "$(git status --porcelain -- data/rules)" ]; then
    echo "Rules have been updated."

    git add data/rules
    git commit -m "rules: Automatic rules update [noissue]"
    git push origin HEAD:$CI_COMMIT_BRANCH
fi

if [ "$(git status --porcelain)" ]; then
    echo "There were index changes."

    BRANCH_NAME="autoupdate"
    git checkout -B $BRANCH_NAME HEAD
    git add --all
    git commit -m "rules: Automatic index update [noissue]"
    git push --force origin $BRANCH_NAME

    # Note that this will fail if an MR from a previous run is still open.
    glab mr create --fill --squash-before-merge --remove-source-branch --yes
fi
