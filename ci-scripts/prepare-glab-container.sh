#!/bin/sh

set -eux

apk add bash jq git nodejs npm
git config --global user.email "extensions-release-squad@eyeo.com"
git config --global user.name "Eyeo Gitlab Automaton"
